package com.kpfu.software.testing;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LoginUtilTest {

    private LoginUtil loginUtil;

    @Before
    public void setUp() throws Exception {
        loginUtil = new LoginUtil();
    }

    @Test
    public void isStrongLogin() {
        assertFalse(loginUtil.isStrongLogin(null));
        assertFalse(loginUtil.isStrongLogin("Кириллица"));
        assertFalse(loginUtil.isStrongLogin("1234"));

        assertTrue(loginUtil.isStrongLogin("SafeLogin"));
        assertTrue(loginUtil.isStrongLogin("Strong7"));
    }

    @Test(expected = ClassCastException.class)
    public void castExceptionCheck() {
        loginUtil.isStrongLogin((String) new Object());
    }
}