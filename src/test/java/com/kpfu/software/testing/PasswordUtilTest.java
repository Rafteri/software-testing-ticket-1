package com.kpfu.software.testing;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordUtilTest {

    private PasswordUtil passwordUtil;

    @Before
    public void setUp() throws Exception {
        passwordUtil = new PasswordUtil();
    }

    @Test
    public void isStrongPassword() {
        assertFalse(passwordUtil.isStrongPassword(null));
        assertFalse(passwordUtil.isStrongPassword("onlylowercase"));
        assertFalse(passwordUtil.isStrongPassword("ONLYUPPERCASE"));
        assertFalse(passwordUtil.isStrongPassword("8888888888888"));
        assertFalse(passwordUtil.isStrongPassword("withoutNUMBERS"));
        assertFalse(passwordUtil.isStrongPassword("onlylowerandnum8er"));
        assertFalse(passwordUtil.isStrongPassword("ONLYUPPERANDNUM8ER"));
        assertFalse(passwordUtil.isStrongPassword("12345"));

        assertTrue(passwordUtil.isStrongPassword("somePass77"));
    }

    @Test(expected = ClassCastException.class)
    public void castExceptionCheck() {
        passwordUtil.isStrongPassword((String) new Object());
    }
}