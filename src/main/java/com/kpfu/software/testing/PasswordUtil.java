package com.kpfu.software.testing;

import java.util.regex.Pattern;

public class PasswordUtil {

    public static final Pattern STRONG_PASSWORD_REGEX = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$");

    public boolean isStrongPassword(final String password) {
        return password != null && STRONG_PASSWORD_REGEX.matcher(password).matches();
    }
}
