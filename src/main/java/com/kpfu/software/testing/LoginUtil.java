package com.kpfu.software.testing;

import java.util.regex.Pattern;

public class LoginUtil {

    public static final Pattern STRONG_LOGIN_REGEX = Pattern.compile("^[A-Za-z\\d]{5,}$");

    public boolean isStrongLogin(final String login) {
        return login != null && STRONG_LOGIN_REGEX.matcher(login).matches();
    }
}
